module gitlab.com/ilya.sheidin/go-logivice

go 1.14

require (
	github.com/lib/pq v1.3.0 // indirect
	gitlab.com/ilya.sheidin/go-logivice/api v0.0.0-20200328131335-b54866419212
	gitlab.com/ilya.sheidin/go-logivice/api/controllers v0.0.0-20200328131335-b54866419212 // indirect
	gitlab.com/ilya.sheidin/go-logivice/api/middlewares v0.0.0-20200328131335-b54866419212 // indirect
	gitlab.com/ilya.sheidin/go-logivice/api/models v0.0.0-20200328131335-b54866419212 // indirect
	gitlab.com/ilya.sheidin/go-logivice/api/security v0.0.0-20200328131335-b54866419212 // indirect
	gitlab.com/ilya.sheidin/go-logivice/api/security/auth v0.0.0-20200328131335-b54866419212 // indirect
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
)
