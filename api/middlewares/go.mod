module gitlab.com/ilya.sheidin/go-logivice/api/middlewares

go 1.14

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	gitlab.com/ilya.sheidin/go-logivice/api/security/auth v0.0.0-20200327162357-d9ddb097554e
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
)
