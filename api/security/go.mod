module gitlab.com/ilya.sheidin/go-logivice/api/security

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
)
