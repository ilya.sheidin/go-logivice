module gitlab.com/ilya.sheidin/go-logivice/api/security/auth

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
)
