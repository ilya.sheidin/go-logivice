package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Role struct {
	gorm.Model
	ID        uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Name      string    `gorm:"size:191;not null;unique" json:"name"`
	DisplayName  string    `gorm:"size:191;not null;" json:"display_name"`
	Settings     string    `gorm:"type:json" json:"settings"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	OrderRemarkTypeId uint32
	OrderRemarkType OrderRemarkType `gorm:"association_foreignkey:OrderRemarkTypeId"`
}
