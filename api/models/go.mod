module gitlab.com/ilya.sheidin/go-logivice/api/models

go 1.14

require (
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/jinzhu/gorm v1.9.12
	gitlab.com/ilya.sheidin/go-logivice/api/security v0.0.0-20200327155824-b6d901168f4f
)
