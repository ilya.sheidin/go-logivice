module gitlab.com/ilya.sheidin/go-logivice/api

go 1.14

require (
	github.com/joho/godotenv v1.3.0
	gitlab.com/ilya.sheidin/go-logivice/api/controllers v0.0.0-20200327172155-9f5e95f1dace
	gitlab.com/ilya.sheidin/go-logivice/api/middlewares v0.0.0-20200327172155-9f5e95f1dace // indirect
)
