module gitlab.com/ilya.sheidin/go-logivice/api/controllers

go 1.14

require (
	github.com/aws/aws-sdk-go v1.29.33
	github.com/gin-gonic/gin v1.6.2
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	gitlab.com/ilya.sheidin/go-logivice/api/middlewares v0.0.0-20200327172155-9f5e95f1dace
	gitlab.com/ilya.sheidin/go-logivice/api/models v0.0.0-20200327162357-d9ddb097554e
	gitlab.com/ilya.sheidin/go-logivice/api/security v0.0.0-20200327155824-b6d901168f4f
	gitlab.com/ilya.sheidin/go-logivice/api/security/auth v0.0.0-20200327162357-d9ddb097554e
	gitlab.com/ilya.sheidin/go-logivice/api/utils/formaterror v0.0.0-20200327172155-9f5e95f1dace
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
)
